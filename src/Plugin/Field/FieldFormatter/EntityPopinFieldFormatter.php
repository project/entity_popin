<?php

namespace Drupal\entity_popin\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityDisplayRepository;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'entity_popin_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_popin_field_formatter",
 *   label = @Translation("Entity popin"),
 *   field_types = {
 *     "entity_reference",
 *     "entity_reference_revisions"
 *   }
 * )
 */
class EntityPopinFieldFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityRepository
   */
  protected $entityRepository;

  /**
   * @var \Drupal\Core\Entity\EntityDisplayRepository
   */
  protected $entityDisplayRepository;

  /**
   * QueryStringFieldFormatter constructor.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepository $entityDisplayRepository
   *   EntityDisplayRepository.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityTypeManagerInterface $entity_type_manager, EntityDisplayRepository $entityDisplayRepository) {
    parent::__construct($plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $label,
      $view_mode,
      $third_party_settings);
    $this->entityTypeManager = $entity_type_manager;
    $this->entityDisplayRepository = $entityDisplayRepository;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return \Drupal\Core\Field\FormatterBase|\Drupal\Core\Plugin\ContainerFactoryPluginInterface|\Drupal\entity_popin\Plugin\Field\FieldFormatter\EntityPopinFieldFormatter|static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository'));
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'width' => '640',
        'height' => '480',
        'title' => '',
        'view_mode' => 'default',
        'type' => 'modal',
        'link_text' => '',
        'class' => '',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    // Dialog.
    $form['width'] = [
      '#title' => t("Window width"),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('width'),
    ];
    $form['height'] = [
      '#title' => t("Window height"),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('height'),
    ];
    $form['title'] = [
      '#title' => t("Window custom title"),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('title'),
      '#description' => t("Leave blank to use the target entity label"),
    ];
    $form['type'] = [
      '#title' => t('Dialog type'),
      '#type' => 'select',
      '#options' => ['modal' => 'Modal dialog', 'dialog' => 'Non modal dialog', 'off_canvas' => 'Off canvas dialog'],
      '#default_value' => $this->getSetting('type'),
    ];

    // Entity
    $form['view_mode'] = [
      '#title' => t('Referenced entity view mode'),
      '#type' => 'select',
      '#options' => $this->getViewModes($this->fieldDefinition->getItemDefinition()->getSetting('target_type')),
      '#default_value' => $this->getSetting('view_mode'),
    ];

    // Link
    $form['link_text'] = [
      '#title' => t("Link text"),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('link_text'),
      '#description' => t("Leave blank to use the target entity label"),
    ];
    $form['class'] = [
      '#title' => t("Extra classes"),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('class'),
      '#description' => t("Add classes separated by commas."),
    ];
    return parent::settingsForm($form, $form_state) + $form;
  }

  /**
   * GetViewModes.
   *
   * @param string $entityType
   *   EntityType.
   *
   * @return array
   *   View mode array.
   */
  protected function getViewModes(string $entityType) {
    $viewModes = $this->entityDisplayRepository->getViewModeOptions($entityType);
    $viewModesSanitized = [];
    if (!empty($viewModes)) {
      foreach ($viewModes as $id => $label) {
        $viewModesSanitized[$id] = $id == 'default' ? 'default' : $label;
      }
    }
    return $viewModesSanitized;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = t("Window width") . " : " . $this->getSetting('width');
    $summary[] = t("Window height") . " : " . $this->getSetting('height');
    $summary[] = t("Window title") . " : " . (!empty($this->getSetting('title')) ? $this->getSetting('title') : 'target entity title');
    $summary[] = t("Entity view mode") . " : " . $this->getSetting('view_mode');
    $summary[] = t("Dialog type") . " : " . $this->getSetting('type');
    $summary[] = t("Link text") . " : " . $this->getSetting('link_text');
    $summary[] = t("Extra classes") . " : " . $this->getSetting('class');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $targetType = $this->fieldDefinition->getItemDefinition()->getSetting('target_type');
    foreach ($items as $delta => $item) {
      $id = $item->entity->id();
      if ($id && $targetType) {
        $elements[$delta] = [
          '#markup' => $this->buildMarkup($targetType, $id),
        ];
      }
    }
    return $elements;
  }

  /**
   * @param $targetType
   * @param $id
   *
   * @return string
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function buildMarkup($targetType, $id) {
    $width = $this->getSetting('width');
    $height = $this->getSetting('height');
    $title = $this->getSetting('title');
    $viewMode = $this->getSetting('view_mode');
    $linkText = $this->getSetting('link_text');
    $classes = $this->getSetting('class');
    $classes = $this->sanitizeClass($classes);
    $linkText = empty($linkText) ? $this->entityTypeManager->getStorage($targetType)->load($id)->label() : $linkText;
    $title = !empty($title) ? $title : $linkText;
    $options = htmlentities(json_encode(["width" => $width, "height" => $height, "title" => $title]));
    $dialogType = [
      'modal' => 'data-dialog-type="modal"',
      'dialog' => 'data-dialog-type="dialog"',
      'off_canvas' => 'data-dialog-renderer="off_canvas" data-dialog-type="dialog"',
    ];
    $html = sprintf('<div class="popin%s">', ' ' . $classes);
    $html .= sprintf('<a class="use-ajax" data-dialog-options="%s" %s',
      $options,
      $dialogType[$this->getSetting('type')]);
    $html .= sprintf(' href="/entity-popin/render/%s/%s/%s">%s</a></div>', $targetType, $id, $viewMode, $linkText);
    return $html;
  }

  /**
   * @param $classes
   *
   * @return string|void
   */
  protected function sanitizeClass($classes) {
    $classArray = !empty($classes) ? explode(',', $classes) : '';
    if (!empty($classArray)) {
      foreach ($classArray as $index => $class) {
        $classArray[$index] = trim($class);
      }
      return ' ' . implode(' ', $classArray);
    }
    return '';
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    return nl2br(Html::escape($item->value));
  }

}
