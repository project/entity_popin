<?php

namespace Drupal\entity_popin\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManager;

/**
 * Class EntityPopinController
 *
 * @package Drupal\entity_popin\Controller
 */
class EntityPopinController extends ControllerBase {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * EntityPopinController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   */
  public function __construct(EntityTypeManager $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * @param $entityType
   * @param $id
   * @param string $viewMode
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getContent($entityType, $id, $viewMode = 'default') {
    $entity = $this->entityTypeManager->getStorage('node')->load($id);
    return $entity ? $this->entityTypeManager->getViewBuilder($entityType)->view($entity, $viewMode) : [];
  }

}
